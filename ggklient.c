#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct client_struct {
  char nick[10];
  char text[256];
  char date[30];
  int client_fd;
  int receiver_fd;
  int cmd;
  int status;
  struct sockaddr_in client_address;
};

int main(int argc, char* argv[]) {
    struct sockaddr_in srvaddr;
    struct hostent* addrent;
    char buf[100];
    int i, n, connection, choice;

    if (argc != 3) {
        fprintf(stderr, "usage: %s server port\n", argv[0]);
        return 1;
    }

    addrent = gethostbyname(argv[1]);
    int fd = socket(PF_INET, SOCK_STREAM, 0);  //SOCK_STREAM - TCP, SOCK_DGRAM - UDP

    srvaddr.sin_family = PF_INET;
    srvaddr.sin_port = htons(atoi(argv[2]));
    memcpy(&srvaddr.sin_addr.s_addr, addrent->h_addr, addrent->h_length);

    struct client_struct* client = malloc(sizeof(struct client_struct));
    struct client_struct* response = malloc(sizeof(struct client_struct));

    connection = connect(fd, (struct sockaddr*)&srvaddr, sizeof(srvaddr));

    if (connection == 0) {
        printf("Udało się nawiązać połączenie z serwerem.\n");

        do {
            client->cmd = 4;
            write(fd, client, sizeof(struct client_struct));
            read(fd, response, sizeof(struct client_struct));
            if(response->status == 4) {
                printf("masz wiadomość!\n");
            }

            printf("Wybierz opcję z menu: \n1. logowanie\n2. lista użytkowników\n3. wyślij wiadomość do użytkownika\ninne: wyjdź\n");
            scanf("%d", &choice);
            system ( "clear" );

            switch(choice) {
                case 1:
                    printf("Logowanie. Podaj swój nick: ");
                    scanf("%s", client->nick);
                    client->cmd = 1;
                    write(fd, client, sizeof(struct client_struct));
                    read(fd, response, sizeof(struct client_struct));
                    switch(response->status) {
                        case 0:
                            printf("Pomyślnie zalogowano użytkownika\n");
                            break;

                        case 1:
                            printf("Nick już zajęty\n");
                            break;

                        case 2:
                            printf("Użytkownik już zalogowany\n");
                            break;

                        case 3:
                            printf("Błąd serwera\n");
                            break;

                        default:
                            printf("Niezidentyfikowany błąd\n");
                            break;
                    }
                    break;

                case 2:
                    printf("lista użytkowników:\n");
                    client->cmd = 2;
                    write(fd, client, sizeof(struct client_struct));
                    read(fd, response, sizeof(struct client_struct));
                    printf("%s\n", response->text);
                    break;

                case 3:
                    printf("wiadomość do użytkownika\n");
                    client->cmd = 3;
                    printf("podaj fd użytkownika: ");
                    scanf("%d", &client->receiver_fd);
                    printf("podaj wiadomość: ");
                    scanf("%s", client->text);
                    write(fd, client, sizeof(struct client_struct));
                    break;

                default:
                    choice = 9;
                    client->cmd = 9;
                    write(fd, client, sizeof(struct client_struct));
                    read(fd, response, sizeof(struct client_struct));
                    switch(response->status) {
                        case 0:
                            printf("Pomyślnie wylogowano");
                            break;

                        case 8:
                            printf("Nie jesteś zalogowany");
                            break;

                        default:
                            printf("Niezidentyfikowany błąd");
                            break;
                    }
                    break;
            }
            printf("\n");
        } while(choice != 9);

        close(fd);
    }
    else {
        printf("Nie można nawiązać połączenia z serwerem");
    }

    free(client);
}
