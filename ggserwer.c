#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>

struct logged {
    char nick[10];
    int fd;
    char text[256];
};

struct client_struct {
    char nick[10];
    char text[256];
    char date[30];
    int client_fd;
    int receiver_fd;
    int cmd;
    int status;
    struct sockaddr_in client_address;
};

struct thread_data {
    struct client_struct client;
    struct logged (*logged_array)[];
};

//return -2 if this user with fd is logged in
//return -1 if nick is already taken
//return index for first not taken index in array
//else return -3
int return_user_in_array(struct logged *logged_array, char nick[10], int fd) {
    int i;
    for(i = 0; i < 18; i++) {
        if(logged_array[i].fd == fd) {
            return -2;
        }
        if(!strcmp(logged_array[i].nick, nick)) {
            return -1;
        }
    }
    for(i = 17; i >= 0; i--) {
        if(logged_array[i].fd == -1) {
            return i;
        }
    }
    return -3;
}

//return index if user is logged in or -1 otherwise
int logged_in(struct logged *logged_array, int fd) {
    int i;
    int to_return = -1;
    for(i = 0; i < 18; i++) {
        if(logged_array[i].fd == fd) {
            to_return = i;
            break;
        }
    }
    return to_return;
}

//return 8 if user is not logged
//return 1 if nick is already taken
//return 0 if new nick was set
int set_new_login(struct logged logged_array[18], int fd, char nick[10]) {
    int i;
    int logged = logged_in(logged_array, fd);
    if(logged == 0) {
        return 8;
    }
    for(i = 0; i < 18; i++) {
        if(!strcmp(logged_array[i].nick, nick)) {
            return 1;
        }
    }
    for(i = 0; i < 18; i++) {
    if(logged_array[i].fd == fd) {
        strcpy(logged_array[i].nick, nick);
        return 0;
        }
    }
    return 0;
}

void* cthread(void* arg) {
    struct thread_data* t_data = (struct thread_data*)arg;
    struct logged (*logged_array)[] = t_data->logged_array;

    struct client_struct* client = &t_data->client;
    struct client_struct* request = malloc(sizeof(struct client_struct));
    struct client_struct* response = malloc(sizeof(struct client_struct));

    int user_in_array;

    while(1) {
        int status = read(client->client_fd, request, sizeof(struct client_struct));
        request->client_fd = client->client_fd;
        if (status != -1) {
            switch(request->cmd) {
                case 1:
                    //login user
                    printf("");     //don't know why it should be here but otherwise it throws error 'expected expression'
                    user_in_array = return_user_in_array(*logged_array, request->nick, request->client_fd);
                    if (user_in_array < 0) {
                        response->status = abs(user_in_array);
                    } else {
                        response->status = 0;
                        strcpy((*logged_array)[user_in_array].nick, request->nick);
                        (*logged_array)[user_in_array].fd = request->client_fd;
                    }
                    write(client->client_fd, response, sizeof(struct client_struct));
                    break;

                case 2:
                    //send users list
                    printf("");
                    char user_fd[2];
                    strcpy(response->text, "");
                    for(int i=0; i<18; i++) {
                        if(strcmp((*logged_array)[i].nick, "")) {
                            strcat(response->text, (*logged_array)[i].nick);
                            strcat(response->text, " ");
                            sprintf(user_fd, "%d", (*logged_array)[i].fd);
                            strcat(response->text, user_fd);
                            strcat(response->text, "; ");
                        }
                    }
                    write(client->client_fd, response, sizeof(struct client_struct));
                    break;

                case 3:
                    //save message to user array
                    printf("");
                    user_in_array = logged_in(*logged_array, request->receiver_fd);
                    strcpy((*logged_array)[user_in_array].text, request->text);
                    break;

                case 4:
                    //check if user has messages
                    printf("");
                    user_in_array = logged_in(*logged_array, request->client_fd);
                    response->status = 0;
                    if(user_in_array != -1) {
                        if (strcmp((*logged_array)[user_in_array].text, "")) {
                            response->status = 4;
                        };
                    };
                    write(client->client_fd, response, sizeof(struct client_struct));
                    break;

                case 9:
                    //logout user
                    printf("");
                    user_in_array = logged_in(*logged_array, request->client_fd);
                    if (user_in_array >= 0) {
                        strcpy((*logged_array)[user_in_array].nick, "");
                        (*logged_array)[user_in_array].fd = -1;
                        response->status = 0;
                    } else {
                        response->status = 8;
                    }
                    write(client->client_fd, response, sizeof(struct client_struct));
                    break;
            }
        }
        user_in_array = -1;
    }
    printf("test\n");
    close(client->client_fd);
    free(client);
    free(request);
    return 0;
}

int main(int argc, char* argv[]) {
    pthread_t tid;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    char buf[100];
    int i, n, addr_size, on=1;
    struct logged logged_array[18];

    for (i = 0; i < 18; i++) {
        logged_array[i].fd = -1;
        strcpy(logged_array[i].nick, "");
        strcpy(logged_array[i].text, "");
    }

    int s_fd = socket(PF_INET, SOCK_STREAM, 0);   //SOCK_STREAM - TCP, SOCK_DGRAM - UDP

    setsockopt(s_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on)); // zmiana opcji dla gniazda

    server_address.sin_family = PF_INET;
    server_address.sin_port = htons(1234);
    server_address.sin_addr.s_addr = INADDR_ANY;    // inet_addr("0:0:0:0") = INADDR_ANY

    i = bind(s_fd, (struct sockaddr*)&server_address, sizeof(server_address));  // związanie gniazda z adresem lokalnej maszyny
    listen(s_fd, 10); // drugi argument - rozmiar kolejki oczekiwania klientow

    while(1) {
        struct client_struct* client = malloc(sizeof(struct client_struct));
        struct thread_data* t_data = malloc(sizeof(struct thread_data));
        addr_size = sizeof(client_address);
        client->client_fd = accept(s_fd, (struct sockaddr*)&client->client_address, (unsigned int *)&addr_size); // trzeci argument - wskaznik na rozmiar struktury! funkcja zwraca nowe gniazdo
        t_data->client = *client;
        t_data->logged_array = &logged_array;
        pthread_create(&tid, NULL, cthread, t_data);
        pthread_detach(tid);
    };

    close(s_fd);
}

